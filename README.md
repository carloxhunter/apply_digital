### How to run:
Be sure to
- Have docker installed (with docker compose)
- If running in Linux environment:
    - ``bash easy.sh``
    - Other (or Linux as well): this should be ordered, I always run the compose down and rm -f in order to be sure is fresh deploy. 
        - ``docker compose down``
        - ``docker compose rm -f``
        - ``docker rmi apply_digital-ad_task``
        - ``docker compose up --build --attach ad_task ``
    - If for some reason you have a bug, just re-run the commands or the easy.sh

### How to use the API:
First, you have to authenticate with POST to the endpoint ``http://IP:8000/api/token/`` in order to get the access (and refresh) token. I provide an admin user with credentials that must be included in the payload:
        
            {
            "username":"admin",
            "password":"admin"
            }
Here's a screenshot: \
![image info](./images/login.png) \
I use insomnia, be sure to have the content type setting in your headers  \
![image info](./images/contenttype.png) \
If token expires, just login again or ask for refresh: ``http://IP:8000/api/token/refresh/`` \
![image info](./images/refresh.png) \
### How to include the token:
Once we have the token in our hands, we have to provide it in the headers as follows: \
![image info](./images/bearertoken.png) \
or (how I usually do it in insomnia via authorization setting): \
![image info](./images/insomniatoken.png)


### Task Endpoints
There are 2 endpoints in the API:
- GET: ``http://IP:8000/news/``: Where we ask the news
    Can accept the next parameters:
    - title: filter by title
    - author: filter by author name
    - created_at: filter by month, only allows next strings: ``january, july, february, august, march, september, april, october, may, november, june, december`` and one at time.
    - tag: provide N tags as many are you want for example tag=hi, tag1=bye, tag3=zero, but keep in mind that if a new hasn't the exact tags (the N you provide) the result will be probably empty.
    - Here is a full example: ``http://IP:8000/news/?title=Musings on Python Type Hints&author=sgeisenh&created_at=october&tag=story&tag1=author_sgeisenh`` in this case I set 2 tags that are provided by this new, therefore it works, but I recommend using just one.
    - the result can be 200, if everything went ok, with a payload or 404 with empty payload in case there isnt any coincidence with the parameters provided.
    - If no parameters are provided, it returns 5 randoms news.
- PATCH: ``http://IP:8000/news/hide/``: Where we delete (Hide) the news
    - Provide the objectID of the new you want to delete (hide) as follows: \
    ![image info](./images/hidenew.png)
    - the result can be 200, if everything went ok, with the new you changed as payload or 404 with empty payload in case there isn't any coincidence with the objectID you provided. 
    - How to test this: well the easiest way is to ask for the news get method, save the title or (and) author of a new, then you can ask again with those filters provided to check that works, or directly send the objectID to ``http://IP:8000/news/hide/`` and finally ask for it with the parameters you saved and the get method and should return empty, not found.

### How did I tackle the remove problem?
I thought the simplest way to avoid the new appears again was adding a field that prevent from being shown.  As we don't save duplicates, we only save the news with fresh ``objectID`` if the server receive a duplicated new that already exist, and we removed it via setting the ``hidden=True`` it won't replace therefore will keep its ``hidden status`` and won't be shown in further gets.
### Do you need to re-populate the db?
No, there is included a JSON with 2 main objects (among others that aren't important).
- Admin account to login.
- 1 New for testing if the scheduler hasn't added tasks yet.
And the docker compose file adds it automatically at server start.
### How did I tackle scheduling problem?
I remember that I worked in a small company where the backend team had to do this (with Django too), and I remembered they used Celery for it, so I knew what to use right away.
### Testing, did I cover the 30 %?
To be honest, I don't know which tool you use for this, but I used ``coverage`` (``pip install coverage``) and ran the test the following way ``coverage run manage.py test`` and then ran ``coverage html``, where is shown that the 49 % is covered. \
![image info](./images/coverage.png) \
I remove empty and 100 % covered files in order to obtain the most honest test coverage possible. The report is included in a folder called ``htmlcov`` specifically open the ``index.html``.



Best regards, Carlos.
