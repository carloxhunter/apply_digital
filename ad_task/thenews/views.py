from django.shortcuts import render
from rest_framework.views import APIView
from thenews.models import TheNew
import random
from rest_framework.permissions import IsAuthenticated
from rest_framework.response import Response
from thenews.serializers import TheNewSerializer
from rest_framework import status
from django.db.models import Q
import calendar
month_lookup = {'january':1, 'july':7,'february':2,'august':8,'march':3,'september':9,
'april':4,'october':10,'may':5,'november':11,'june':6,'december':12}

# Create your views here.
class GetAllNews(APIView):
    permission_classes = [IsAuthenticated]
    def get(self, request):
        query_params = request.GET
        response = Response()
        allowed_filters = ['author', 'title']
        filtered_params = dict((k, query_params[k]) for k in allowed_filters if k in query_params)
        tags = []
        for key in query_params.keys():
            if key.startswith('tag'):
                tags.append(query_params[key])
        if 'created_at' in query_params:
            if query_params['created_at'] in month_lookup.keys():
                month = query_params['created_at']
            else: month = None
        else: month = None

        #why this? well because I decided to not use _tags in the db avoiding special chars
        #therefore I have to transform from _tags to tags
        #we take (if exist the month before)
        
        #we dont show the hidden results
        filtered_params['hidden'] = False
        query_filtered = TheNew.objects.filter(**filtered_params)
        if month:
            query_filtered=query_filtered.filter(created_at__month=month_lookup[month])
        
        if tags:
            query_filtered=query_filtered.filter(tags__contains=tags)

        matches = query_filtered.count()
        if matches <= 0:
            response.status_code = status.HTTP_404_NOT_FOUND
            response.data = {}
            return response
        elif matches > 5:
            news_list = list(query_filtered)
            random_5 = random.sample(news_list, 5)
            query_serializer = TheNewSerializer(random_5, many=True)
        else:
            news_list = list(query_filtered)    
            query_serializer = TheNewSerializer(query_filtered, many=True)
        
        response.status_code = status.HTTP_200_OK
        response.data = query_serializer.data
        return response
            
        
        
class RemoveItem(APIView):
    permission_classes = [IsAuthenticated]
    def patch(self, request):
        response = Response()
        bodyparams = request.data
        if "objectID" not in bodyparams:
            response.status_code = status.HTTP_400_BAD_REQUEST
            response.data = {'please specify which new you would like to remove with objectID parameter, for example {"objectID":"1"} '}
            return response
        else:
            #actually is not many, since is only one
            matches = TheNew.objects.filter(objectID = bodyparams["objectID"]).count()
            if matches > 0:
                thenew = TheNew.objects.get(objectID = bodyparams["objectID"])
                thenew.hidden = True
                thenewserielizer = TheNewSerializer(thenew)
                thenew.save()
                response.status = status.HTTP_200_OK
                response.data = thenewserielizer.data
                return response
            else: 
                response.status_code = status.HTTP_404_NOT_FOUND
                response.data = {f'object with objectID: {bodyparams["objectID"]} not found'}
                return response
        
        
