# Generated by Django 4.0.8 on 2022-10-16 23:31

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('thenews', '0002_thenew_hidden'),
    ]

    operations = [
        migrations.AlterField(
            model_name='thenew',
            name='story_text',
            field=models.TextField(blank=True, null=True),
        ),
    ]
