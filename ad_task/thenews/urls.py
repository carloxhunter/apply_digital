from django import views
from django.contrib import admin
from django.urls import path, include
from rest_framework_simplejwt import views as jwt_views
from rest_framework.urlpatterns import format_suffix_patterns
from thenews import views



urlpatterns = [
path(r'news/', views.GetAllNews.as_view(), name='api_fetch_test'),
path(r'news/hide/', views.RemoveItem.as_view(), name='remove_new')
]

urlpatterns = format_suffix_patterns(urlpatterns)