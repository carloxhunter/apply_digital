from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.forms import BooleanField


class TheNew(models.Model):
    created_at = models.DateTimeField('date published')
    title = models.CharField(max_length=250)
    url = models.CharField(max_length=250, null=True, blank=True)
    author = models.CharField(max_length=250)
    points = models.IntegerField()
    story_text = models.TextField(null=True, blank=True)
    comment_text = models.CharField(max_length=250, null=True, blank=True)
    num_comments = models.IntegerField()
    story_id = models.CharField(max_length=250, null=True, blank=True)
    story_title = models.CharField(max_length=250, null=True, blank=True)
    story_url = models.CharField(max_length=250, null=True, blank=True)
    parent_id = models.CharField(max_length=250, null=True, blank=True)
    created_at_i = models.IntegerField()
    objectID = models.CharField(max_length=250)
    tags = ArrayField(models.CharField(max_length=250), null=True, blank=True)
    hidden = models.BooleanField(default=True)
