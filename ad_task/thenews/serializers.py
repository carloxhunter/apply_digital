from rest_framework import serializers
from thenews.models import TheNew


class TheNewSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = TheNew
        exclude = ('hidden',)
