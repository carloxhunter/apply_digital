from django.http import QueryDict
from django.test import TestCase
from django.test.client import RequestFactory

from thenews.views import GetAllNews, RemoveItem
# Create your tests here.



class CoverageTesting(TestCase):
    @classmethod
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
    def test_getParams(self):
        response = self.client.patch('news/hide/')
        self.assertEqual(404, response.status_code)
    def test_defparams(self):
        month_lookup = {'january':1, 'july':7,'february':2,'august':8,'march':3,'september':9,
'april':4,'october':10,'may':5,'november':11,'june':6,'december':12}
        response = self.client.get('/news/', {'author': 'jiro'})
        query_params=response.request['QUERY_STRING']
        query_params = QueryDict(query_params)
        allowed_filters = ['author', 'title'] 
        filtered_params = dict((k, query_params[k]) for k in allowed_filters if k in query_params) 
        tags = []
        for key in query_params.keys():
            if key.startswith('tag'):
                tags.append(query_params[key])
        if 'created_at' in query_params:
            if query_params['created_at'] in month_lookup.keys():
                month = query_params['created_at']
            else: month = None
        else: month = None
        filtered_params['hidden'] = False
        self.assertEqual(2, len(filtered_params))



