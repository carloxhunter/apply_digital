#from demoapp.models import Widget
import requests
import django
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

from thenews.models import TheNew
#og
#news_url = 'https://hn.algolia.com/api/v1/search_by_date?query=python'
#asuming we only want stories, not comments
news_url='https://hn.algolia.com/api/v1/search_by_date?query=python&tags=story'

from celery import shared_task

@shared_task(bind=True, ignore_result=True, name = "fetch_news")
def fetch_news(self):
    try:
        r = requests.get(url = news_url)
        data = r.json()
        for idata in data["hits"]:
            if not TheNew.objects.filter(objectID = idata["objectID"]).exists():
                print(f'Inserting objectID {idata["objectID"]}')
                new_new = TheNew(created_at = idata["created_at"], title = idata["title"], url = idata["url"],
                author=idata["author"], points = idata["points"], story_text = idata["story_text"], 
                comment_text = idata["comment_text"], num_comments = idata["num_comments"],
                story_id = idata["story_id"], story_title = idata["story_title"], story_url = idata["story_url"],
                parent_id = idata["parent_id"], created_at_i = idata["created_at_i"], objectID = idata["objectID"],
                tags = idata["_tags"], hidden = False)
                #print(f'ids: {idata["objectID"]}')
                #print(f'tags: {idata["_tags"]}')
                new_new.save()
            else:
                print(f'{idata["objectID"]} objectID already in the database')
        return data
    except Exception as e: 
        #not good practise, por testing purposes only
        raise e
    