from django.test import TestCase
from django.test.client import RequestFactory
from thenews.models import TheNew
import requests

class TaskTesting(TestCase):
    @classmethod
    def setUp(self):
        # Every test needs access to the request factory.
        self.factory = RequestFactory()
    def test_check_api_code(self):
        news_url='https://hn.algolia.com/api/v1/search_by_date?query=python&tags=story'
        r = requests.get(url = news_url)
        self.assertEqual(200,r.status_code)
