import os
#this HAVE to be here in order to load settings before starting celery!
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'ad_task.settings')
#from celery import Celery
from celery import Celery, shared_task
from celery.schedules import crontab
from datetime import timedelta
from .tasks import *
import django
# Set the default Django settings module for the 'celery' program.


#os.environ['DJANGO_SETTINGS_MODULE'] = 'ad_task.settings'
#django.setup()

app = Celery('ad_task', broker='amqp://rabbitmq')

# Using a string here means the worker doesn't have to serialize
# the configuration object to child processes.
# - namespace='CELERY' means all celery-related configuration keys
#   should have a `CELERY_` prefix.
app.config_from_object('django.conf:settings', namespace='CELERY')

# Load task modules from all registered Django apps.
app.autodiscover_tasks()


#meanwhile each 5 mins
app.conf.beat_schedule = {
    'add-every-1-hour': {
        'task': 'fetch_news',
        'schedule': 3600, #10s test, should be 3600s (1h)
        'args': ()
    },
}