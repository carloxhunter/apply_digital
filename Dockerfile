FROM python:3.8
LABEL Author="elguet5"
# setup environment variable  
# syntax=docker/dockerfile:1
FROM python:3.8
ENV PYTHONDONTWRITEBYTECODE=1
ENV PYTHONUNBUFFERED=1

#directory to store app source code
RUN mkdir /ad_task

#switch to /app directory so that everything runs from here
WORKDIR /ad_task

#copy the app code to image working directory
COPY ./ad_task /ad_task

#let pip install required packages
RUN pip install -r requirements.txt

